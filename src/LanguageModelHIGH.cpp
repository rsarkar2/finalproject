/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */
 
#include "LanguageModelHIGH.hpp"
#include "NgramCollectionHIGH.hpp"

using std::cin;
using std::cout;
using std::string;
using std::endl;
using std::map;
using std::vector;
using std::get;
using std::list;


/* Sets the given file name to the overall file name. */
void LanguageModelHIGH :: setFileName(std::string s){
  this->overallFile = s;
}

/* Sets the user-specified sensitivity level */
void LanguageModelHIGH :: setCh(char c){
  this->ch = c;
}

/* Sets the nGram length (only for low and med sensitivity levels) */
void LanguageModelHIGH :: setNgramLen(int n){
  this->nGramLen = n;
}

/* Adds ngrams based on the sentences that are fed to it */
void LanguageModelHIGH :: addNGramsHIGH(std::list<std::string> words){

  if(!( int(words.size()) < 0)){
    std::vector<string> specificNgram;
    list<string>::iterator begin_list = words.begin();

    list<string>::iterator end_list = begin_list;
    specificNgram.push_back(*begin_list); //specificNgram = {I've}

    int nGram = words.size();


    int i = 0;
    while(i < nGram-1){ //nGram = 4
      end_list = std::next(end_list);
      specificNgram.push_back(*end_list);
      i++;
    }

    coll.increment(specificNgram.begin(), specificNgram.end());
  }

  //cout << " ";
}

/* Adds ngrams based on the given nGram length (for med & low sensitivity) */
void LanguageModelHIGH :: addNGramsMED(std::list<string> words){
  // map<vector<string>, map<string, unsigned> > counts;
  //ex. words = {I've, never, seen, a, diamond, in, the, flesh}
  //n = 4, aka want 4-grams.

  if(!( int(words.size()) < nGramLen)){
    std::vector<string> specificNgram;
    list<string>::iterator begin_list = words.begin();
    list<string>::iterator end_list = begin_list;
    specificNgram.push_back(*begin_list); //specificNgram = {I've}

    int i = 1;
    while(i < nGramLen){ //nGram = 4
      end_list = std::next(end_list);
      specificNgram.push_back(*end_list);
      i++;
      
      /* AFTER: specificNgram.push_back(words.at(i));
       * i = 1: specificNgram = {I've, never}
       * i = 2: specificNgram = {I've, never, seen}
       * i = 3: specificNgram = {I've, never, seen, a}
       */
    }

    coll.increment(specificNgram.begin(), specificNgram.end());
    words.pop_front(); //words = {never, seen, a, diamond, in, the, flesh}
    addNGramsMED(words);
  }
}

/* Checks if a given string is a "small word", outputs true if it is, else false.
if the string is 6 or greater, it is automatically not a smal word */
bool LanguageModelHIGH :: checkIfSmall(std::string word){
  string small[] = {"the", "then", "like", "it", "as", "us", "be", "are", "is", "was", "to", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with",
  "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her", "she", "or", "an", "will", "my", "one", 
  "all", "who", "what", "so", "up", "out", "if", "no"}; //these are arranged in order of most used in the english lang

  if (word.length() < 6){
    
    if (word.length() == 5){
      if (word == "there" || word == "their" || word == "these"){
        return true;
      }
    
    }

    else {
      
      for (int i = 0; i < 51; i++){
        if (word == small[i]){
          //cout << "HERE with " << word << "\n";
          return true;
        }
      }
      return false;
    }
  
  }
  
  return false;
}

/* Removes all punctuation from a word and changes any upper case words to lower case, returns the changes string (free of punct & uppercase) */
std::string LanguageModelHIGH :: removePunct(std::string text){

    for (int i = 0, len = text.size(); i < len; i++){
      text[i] = tolower(text[i]);
      if (ispunct(text[i])){
        text.erase(i--, 1);
        len = text.size();
      }
    }
    return text;
}

/* Removes all punctuation from a word, if there is period/exclamation/question mark at the end of the sentence,
then the first letter of that word is capitalized */
std::string LanguageModelHIGH :: punctCheck(std::string text){
  for (int i = 0, len = text.size(); i < len; i++){
      text[i] = tolower(text[i]);
      if (ispunct(text[i])){
        if ( (text[i] == '.' || text[i] == '!' || text[i] == '?') && (i == len-1)){
          text[0] = toupper(text[0]); //makes the first letter upper case if its the end of the sentence
        }
        text.erase(i--, 1); //still get rid of that period whether or not its at the end
        len = text.size();
      }
    }

    return text;
}

/* Reads the single file and sorts each nGram appropriately based on sentence length (high-level sensitivity) */
void LanguageModelHIGH :: readSingleFileHIGH(string fileName){
  std::ifstream fin;
  fin.open(fileName);

  std::list<string> allWords; //all the words in this specific file
  allWords.clear();

  string str; 
  fin >> str;

  while(!fin.eof()){
    str = punctCheck(str);

    if(tolower(str[0]) != str[0]){ //this is the final word of the sentence
      str[0] = tolower(str[0]);
      if (checkIfSmall(str) == false){
        allWords.push_back(str);
        addNGramsHIGH(allWords);
        allWords.clear();
      }
    } else {
      if (checkIfSmall(str) == false){
        allWords.push_back(str);
      }
    }
    fin >> str;
  }

  fin.close();
}

/* Returns the Ngram in string format (using toString from NgramCollectionHIGH */
std::string LanguageModelHIGH :: toString() const{
  return coll.toString();
}

/* Reads the single file and calls NgramMED to store specific nGram length */
void LanguageModelHIGH :: readSingleFileMED(std::string fileName){
  std::ifstream fin;
  fin.open(fileName);

  std::list<string> allWords; //all the words in this specific file
  allWords.clear();

  string str; 
  fin >> str;

  while(!fin.eof()){
    str = removePunct(str);
    
    if (checkIfSmall(str) == false){
      allWords.push_back(str);
    }

    fin >> str;
  }

  addNGramsMED(allWords);
  fin.close();

}



