#include <iostream>
#include <istream>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>

#include "CompareFiles.hpp"

using std::cout;
using std::string;
using std::clock;
using std::endl;


int main(int argc, char * argv[]){

  std::clock_t start;
  double duration;
  start = clock();  

  //argv[0] = compiled file name
  //argv[1] = text file containing all inputs
  //argv[2] = should be s, m, l; or nothing

  if (argc < 2){
    std::cerr << "Error: not enough arguments" << endl;
    return 0; 
  }

  string fileName;
  char choice = 'm';

  if (argc == 2){
    fileName = argv[1];
  } else if (argc == 3){
    fileName = argv[1];
    choice = tolower(*argv[2]);

    if (!(choice == 'h' || choice == 'm' || choice == 'l')){
      std::cerr << "Error: wrong choice" << endl;
      return 1;
    }


  } else {
    std::cerr << "Error: too many arguments" << endl;
    return 1;
  }

  //cout << "FILENAME: " << fileName << ", CHOICE : " << choice;

  CompareFiles overall;
  overall.setFileName(fileName);
  overall.setCh(choice);
  overall.readOverallFile();
  overall.findOverall();


//dont alter this
  duration = (clock() -  start) / (double) CLOCKS_PER_SEC;
  cout << "Duration: " << duration << endl;
//

  return 0;

}

