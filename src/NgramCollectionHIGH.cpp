/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

#include "NgramCollectionHIGH.hpp"

using std::cin;
using std::cout;
using std::string;
using std::endl;
using std::map;
using std::vector;
using std::get;

  /*
  Increase count for NgramCollection representing values from begin up to end
  begin is an iterator to the first word in the Ngram,
  end is an iterator to the end of the Ngram
  */

void NgramCollectionHIGH :: increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end){
  std::vector<string> listy;
  //string word = removePunct(*begin);
  nGramOrder.push_back(*begin);
  
  string start = *begin;
  
  begin++;
  while (begin != end){
    //word = removePunct(*begin);
    listy.push_back(*begin);
    begin++;
  }
  //counts[listy][*begin] += 1;
  counts[start][listy] += 1; 
}

  /*
  Outputs all of the Ngrams together, one per line, without the numerical value at the end
  */

std::string NgramCollectionHIGH :: toString() const{
  std::stringstream ss;
  std::vector<string> listInside;
  string key;
  int size = 0;

  vector<std::pair<string, vector<string>> > recs;

  typedef std::map<std::string, std::map <std::vector<std::string>, int> > overall;
  //std::map<std::string, std::map <std::vector<std::string>, int> > counts;
  for(overall::const_iterator it = counts.begin(); it != counts.end(); it++){
    for(std::map<std::vector<string>, int>::const_iterator it2 = it-> second.begin(); it2 != it -> second.end(); it2++){
      recs.push_back(make_pair(it->first, it2->first));
    }
  }


  for(vector<std::pair<string, vector<string> > >::iterator it = recs.begin(); it != recs.end(); it++){
    key = get<0>(*it);
    listInside = get<1>(*it);
    ss << key << " ";
    size = listInside.size();
    for (int i = 0; i < size; i++){
      ss << listInside.at(i) << " ";
    }

    ss << "\n";
  }

  return ss.str();

}

  /*
  Prints out the interior vector in the order of the file
  */
std::string NgramCollectionHIGH :: interiorVector() const {
  std::stringstream ss;
  for (vector<string> ::const_iterator iter = nGramOrder.begin(); iter != nGramOrder.end(); iter++){
    ss << *iter << "_";
  }

  return ss.str();
}





