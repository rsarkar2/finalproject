/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

#include "catch.hpp"

#include "CompareFiles.hpp"
#include "NgramCollectionHIGH.hpp"
#include "LanguageModelHIGH.hpp"

#include <string>
#include <map>
#include <vector>
#include <list>

using std::string;
using std::tuple;
using std::vector;
using std::map;

TEST_CASE("adding file of high level, comparing sentences", "[addFileHIGH], [compareSentences], [setCh]"){
  CompareFiles thisFile;

  thisFile.setCh('h');
  thisFile.addFileHIGH("../data/files/urbanGrowth1.txt");
  thisFile.addFileHIGH("../data/files/urbanGrowth2.txt");

  map<vector<string>, int> inner1;
  map<vector<string>, int> inner2;

  vector<string> vect1;
  vector<string> vect2;

  vect1.push_back("serafina");
  vect1.push_back("honeygrow");
  vect1.push_back("pizzaria");
  vect1.push_back("nolan");

  vect2.push_back("honeygrow");
  vect2.push_back("nolan");
  vect2.push_back("chipotle");

  inner1[vect1] = 3;
  inner2[vect2] = 1;

  int output = thisFile.compareSentences(inner1, inner2);

  REQUIRE(output == 1);

  vect1.pop_back();
  vect1.pop_back();
  vect1.pop_back();

  inner1[vect1] = 3;
  output = thisFile.compareSentences(inner1, inner2);

  REQUIRE(output == 1);
}

TEST_CASE("adding file of med level, comparing interior", "[addFileMED], [compareInterior], [setCh]"){
  CompareFiles thisFile;

  thisFile.setCh('h');
  thisFile.addFileMED("../data/files/urbanGrowth1.txt");
  thisFile.addFileMED("../data/files/urbanGrowth2.txt");

  map<vector<string>, int> inner1;
  map<vector<string>, int> inner2;

  vector<string> vect1;
  vector<string> vect2;

  vect1.push_back("serafina");
  vect1.push_back("honeygrow");
  vect1.push_back("pizzaria");
  vect1.push_back("nolan");

  vect2.push_back("honeygrow");
  vect2.push_back("nolan");
  vect2.push_back("chipotle");

  inner1[vect1] = 3;
  inner2[vect2] = 1;

  bool output = thisFile.compareInterior(inner1, inner2);

  REQUIRE(output == false);

  vect1.pop_back();
  vect1.pop_back();
  vect1.pop_back();

  inner1[vect1] = 3;
  output = thisFile.compareInterior(inner1, inner2);

  REQUIRE(output == false);
}









