/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

 
#include "CompareFiles.hpp"

#include "LanguageModelHIGH.hpp"
#include "NgramCollectionHIGH.hpp"

#include <tuple>


using std::cin;
using std::cout;
using std::string;
using std::endl;
using std::map;
/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

using std::vector;
using std::get;
using std::list;
using std::endl;
using std::cerr;

/* sets the overall fileName (overallFile) */ 
void CompareFiles :: setFileName(std::string s){
  this->overallFile = s;
}

/* sets the character (whatever the user specifies) */
void CompareFiles :: setCh(char c){
  this->ch = c;
}

/* 
 this stores the ngrams in sentences, so they all are of different size 
 it depends on where the sentence ends - high level sensitivity
*/
void CompareFiles :: addFileHIGH(std::string file){
  string name = file;
  LanguageModelHIGH thisModel;

  std::ifstream fin; 
  fin.open(file); ///ADD !FIN.OPEN


  if (!fin.is_open()){ 
    cerr << "Error: bad file name." << std::endl;

  }

  thisModel.setFileName(name);
  thisModel.readSingleFileHIGH(name);
  thisModel.setCh('h');
  
  allFiles.push_back(make_pair(name, thisModel.coll));

  //cout << "FILE NAME: " << file << "\n";
  //cout << thisModel.toString();  
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";

  fin.close();
}

/* this stores ngrams for a (each have 3) - medium level sensitivity */
void CompareFiles :: addFileMED(std::string file){

  string name = file;

  LanguageModelHIGH thisModel;

  std::ifstream fin; 
  fin.open(file);

  if (!fin.is_open()){ 
    cerr << "Error: bad file name." << std::endl;

  }

  thisModel.setFileName(name);
  thisModel.setCh('m');
  thisModel.setNgramLen(3);
  thisModel.readSingleFileMED(name);

  allFiles.push_back(make_pair(name, thisModel.coll));

  //cout << "FILE NAME: " << file << "\n";

  //cout << thisModel.toString();
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";

  fin.close();
}

/* this stores ngrams for a (each have 3) - low level sensitivity */
void CompareFiles :: addFileLOW(std::string file){

  string name = file;

  LanguageModelHIGH thisModel;

  std::ifstream fin; 
  fin.open(file);

  if (!fin.is_open()){ 
    cerr << "Error: bad file name." << std::endl;

  }

  thisModel.setFileName(name);
  thisModel.setCh('l');
  thisModel.setNgramLen(3);
  thisModel.readSingleFileMED(name);

  allFiles.push_back(make_pair(name, thisModel.coll));

  //cout << "FILE NAME: " << file << "\n";

  //cout << thisModel.toString();
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";

  fin.close();
}

/* this will read over the overall file and appropriately call either addFileLOW, addFileMED, or addFileHIGH */
void CompareFiles :: readOverallFile(){
 
  std::ifstream fin;

  fin.open(overallFile);

  if (!fin.is_open()){ 
    cerr << "Error: bad file name." << std::endl;

  }
  string str;
  
  fin >> str;
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";

  if (ch == 'm'){
    while(!fin.eof()){
      addFileMED(str);
      fin >> str;
    }
  } else if (ch == 'h'){
    while(!fin.eof()){
      addFileHIGH(str);
      fin >> str;
    }
  } else { //low sensitivity
    while(!fin.eof()){
      addFileLOW(str);
      fin >> str;
    }

  }

  fin.close();

  string sensitivity = "medium sensitivity...";
  int docs = allFiles.size();
  if (ch == 'h'){
    sensitivity = "high sensitivity...";
  } else if (ch == 'l'){
    sensitivity = "low sensitivity...";
  }

  cout << "\t\tScanning through " << docs << " documents with " << sensitivity << " \n\n";
}

/* this compares the interior maps of 2 NgramCollectionHIGH's
will output true if they're the same, else false */
bool CompareFiles :: compareInterior(std::map<std::vector<std::string>, int> map1, std::map <std::vector<std::string>, int > map2){
  //vect1 is always going to equal vect2

  typedef std::map<std::vector<std::string>, int> overall;
  int size = 0;
  vector<string> vect1;
  vector<string> vect2; 

  for (overall::iterator it = map1.begin(); it != map1.end(); it++){
    vect1 = it->first;
    size = vect1.size();

    for(overall::iterator it2 = map2.begin(); it2 != map2.end(); it2++){
      vect2 = it2->first;
      
      for(int i = 0; i < size; i++){
        if (vect1.at(i) != vect2.at(i)){
          break;
        }

        if (i == size-1){
          return true;
        }
      }

    }
  }

  return false;
}

/* this will compare the interior maps of 2 NgramCollectionHIGH's
these are each a sentence in a file
it will output 1 if the sentence is considered plagarised, otherwise it'll output 0 */
int CompareFiles :: compareSentences(std::map<std::vector<std::string>, int> map1, std::map <std::vector<std::string>, int > map2){

  typedef std::map<std::vector<std::string>, int> overall;
  vector<string> vect1;
  vector<string> vect2; 
  int total = 0;
  int wordTotalFirst = 0;

  for (overall::iterator it = map1.begin(); it != map1.end(); it++){
    vect1 = it->first;
    for(overall::iterator it2 = map2.begin(); it2 != map2.end(); it2++){
      vect2 = it2->first;
      //cout << "REPEATED WORDS: ";

      for (int i = 0; i < (int)vect1.size(); i++){
        for (int j = 0; j< (int)vect2.size(); j++){
          
          if (vect1.at(i) == vect2.at(j)){
            total++;
            //cout << vect1.at(i) << " ";
          }
        }
        wordTotalFirst++;

      }
    }
  }

  double numerator = (double)(total)/ (double)wordTotalFirst;
  double denom = (4.0/10.0);

  if (numerator >= denom){
    return 1;
  }
  //cout << "numerator: " << numerator << ", denom: " << denom << "\n";
  return 0;
}

/* this compares sentences rather than 3-length nGrams, so the same words could be mixed up in a sentence - high level sensitivity*/
int CompareFiles :: findPlagarismHIGH(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2){


  std::map<std::vector<std::string>, int> list1;
  std::map<std::vector<std::string>, int> list2;
  int play = 0;
  int runtimes = 0;

  for(int i = 0; i < (int)f1.nGramOrder.size(); i++){
    for (int j = 0; j < (int)f2.nGramOrder.size(); j++){
      list1 = f1.counts[f1.nGramOrder.at(i)];
      list2 = f2.counts[f2.nGramOrder.at(j)];

      play = play + compareSentences(list1, list2);
    }

    runtimes++;

  }

  //cout << "play = " << play << ", runtimes = " << runtimes << "\n";

  double num = (double)play/(double)runtimes;
  double compare = 0.4;

  if (num >= compare){
    cout << file1 << " OR " << file2 << " may have been plagerized.\n";
  }
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";

  return 0;
}

/* this compares 3-length nGrams, if 8% of the file is the same in a row, then returns 1.
else returns 0 */
int CompareFiles :: findPlagarismMED(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2){
  std::vector<std::string> inside1;
  std::map<std::string, unsigned> inside2; 

  int lengthA = f1.nGramOrder.size();
  int lengthB = f2.nGramOrder.size();

  vector<string>::iterator it;

  int copies = 0;

  int j;
  if (lengthA <= lengthB){
    for(int i = 0; i < (int)f1.nGramOrder.size(); i++){
      if (compareInterior(f1.counts[f1.nGramOrder.at(i)], f2.counts[f1.nGramOrder.at(i)]) ){ //2 maps
        
        it = std::find(f2.nGramOrder.begin(),f2.nGramOrder.end(),f1.nGramOrder.at(i));
        j = std::distance(f2.nGramOrder.begin(), it);
        
        while (( (f1.nGramOrder.at(i) == f2.nGramOrder.at(j)) && compareInterior(f1.counts[f1.nGramOrder.at(i)], f2.counts[f1.nGramOrder.at(i)])) ){
          copies++;

          if (copies > .08*lengthA || copies > (.08*lengthB)){ //8% length of document was copied, all of this is in a row
            cout << "Documents " << file1 << " OR " << file2 << " may be plagarized.\n"; 
            return 1; 
          }
          
          if ( (i + 1 != (int)f1.nGramOrder.size()) && (j + 1 != (int)f2.nGramOrder.size()) )  { 
            i++;
            j++;
          } else {
            return 0;
          }
        }
      }
      copies = 0;
        
    }
    return 0;
      
  //cout << "COMPARING: " << file1 << " & " << file2 << ":\n" << copies << " ngrams out of lengthA: " << lengthA << " or lengthB: " << lengthB << "\n";
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";
  

  } else {
  for(int i = 0; i < (int)f2.nGramOrder.size(); i++){
    if (compareInterior(f2.counts[f2.nGramOrder.at(i)], f1.counts[f2.nGramOrder.at(i)]) ){ //2 maps
        
        it = std::find(f1.nGramOrder.begin(),f1.nGramOrder.end(),f2.nGramOrder.at(i));
        j = std::distance(f1.nGramOrder.begin(), it);
        
        while (( (f2.nGramOrder.at(i) == f1.nGramOrder.at(j)) && compareInterior(f2.counts[f2.nGramOrder.at(i)], f1.counts[f2.nGramOrder.at(i)])) ){
          copies++;

          if (copies > .08*lengthA || copies > (.08*lengthB)){ //8% length of document was copied, all of this is in a row
            cout << "Documents " << file1 << " OR " << file2 << " may be plagarized.\n"; 
            return 1; 
          }
          
          if ( (i + 1 != (int)f2.nGramOrder.size()) && (j + 1 != (int)f1.nGramOrder.size()) )  { 
            i++;
            j++;
          } else {
            return 0;
          }
        }
      }
      copies = 0;
        
    }
  //cout << "COMPARING: " << file1 << " & " << file2 << ":\n" << copies << " ngrams out of lengthA: " << lengthA << " or lengthB: " << lengthB << "\n";
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";
    return 0;


  }
}

/* this compares 3-length nGrams, if 15% of the file is the same in a row, then returns 1. 
else returns 0 */
int CompareFiles :: findPlagarismLOW(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2){
  std::vector<std::string> inside1;
  std::map<std::string, unsigned> inside2; 

  int lengthA = f1.nGramOrder.size();
  int lengthB = f2.nGramOrder.size();

  vector<string>::iterator it;

  int copies = 0;

  int j;
  if (lengthA <= lengthB){
    for(int i = 0; i < (int)f1.nGramOrder.size(); i++){
      if (compareInterior(f1.counts[f1.nGramOrder.at(i)], f2.counts[f1.nGramOrder.at(i)]) ){ //2 maps
        
        it = std::find(f2.nGramOrder.begin(),f2.nGramOrder.end(),f1.nGramOrder.at(i));
        j = std::distance(f2.nGramOrder.begin(), it);
        
        while (( (f1.nGramOrder.at(i) == f2.nGramOrder.at(j)) && compareInterior(f1.counts[f1.nGramOrder.at(i)], f2.counts[f1.nGramOrder.at(i)])) ){
          copies++;

          if (copies > .15*lengthA || copies > (.15*lengthB)){ //8% length of document was copied, all of this is in a row
            cout << "Documents " << file1 << " OR " << file2 << " may be plagarized.\n"; 
            return 1; 
          }
          
          if ( (i + 1 != (int)f1.nGramOrder.size()) && (j + 1 != (int)f2.nGramOrder.size()) )  { 
            i++;
            j++;
          } else {
            return 0;
          }
        }
      }
      copies = 0;
        
    }
    return 0;
      
  //cout << "COMPARING: " << file1 << " & " << file2 << ":\n" << copies << " ngrams out of lengthA: " << lengthA << " or lengthB: " << lengthB << "\n";
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";
  

  } else {
  for(int i = 0; i < (int)f2.nGramOrder.size(); i++){
    if (compareInterior(f2.counts[f2.nGramOrder.at(i)], f1.counts[f2.nGramOrder.at(i)]) ){ //2 maps
        
        it = std::find(f1.nGramOrder.begin(),f1.nGramOrder.end(),f2.nGramOrder.at(i));
        j = std::distance(f1.nGramOrder.begin(), it);
        
        while (( (f2.nGramOrder.at(i) == f1.nGramOrder.at(j)) && compareInterior(f2.counts[f2.nGramOrder.at(i)], f1.counts[f2.nGramOrder.at(i)])) ){
          copies++;

          if (copies > .08*lengthA || copies > (.08*lengthB)){ //8% length of document was copied, all of this is in a row
            cout << "Documents " << file1 << " OR " << file2 << " may be plagarized.\n";  
            return 1; 
          }
          
          if ( (i + 1 != (int)f2.nGramOrder.size()) && (j + 1 != (int)f1.nGramOrder.size()) )  { 
            i++;
            j++;
          } else {
            return 0;
          }
        }
      }
      copies = 0;
        
    }
  //cout << "COMPARING: " << file1 << " & " << file2 << ":\n" << copies << " ngrams out of lengthA: " << lengthA << " or lengthB: " << lengthB << "\n";
  //cout << "----------------------------------------------------------------------------------------------------------------------\n";
    return 0;


  }  

  return 0;
}

/* looks through the overall file and calls the correct 'find plagarism' based on what sensitivity level*/
void CompareFiles :: findOverall(){
  // vector< pair<string, NgramCollection> > allFiles;
  
  int num = allFiles.size(); //total number of files that there are
  string file1;
  NgramCollectionHIGH f1;
  string file2;
  NgramCollectionHIGH f2;

  for (std::vector< std::pair<string, NgramCollectionHIGH> > ::iterator it = allFiles.begin(); it != allFiles.end(); it++){
    file1 = get<0>(*it);
    f1 = get<1>(*it);
    
    for (int i = 1; i < num; i++){
      file2 = get<0>(*(it+i));
      f2 = get<1>(*(it+i));
      //cout << "COMPARING: " << file1 << " & " << file2 << "\n";
      if (ch == 'm'){
        findPlagarismMED(file1, f1, file2, f2);
      } else if (ch == 'h'){
        findPlagarismHIGH(file1, f1, file2, f2);
      } else {
        findPlagarismLOW(file1, f1, file2, f2);
      }
    }

    num = num - 1;
  }
}


