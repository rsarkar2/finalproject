/*
 * Name: Reena Sarkar, Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified 12/7/2016
 */

#include "catch.hpp"
#include "NgramCollectionHIGH.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;
using std::endl;

TEST_CASE("toString when empty", "[toString]") {
 NgramCollectionHIGH g2;

 vector <string> v;
 v.push_back("");
 REQUIRE(g2.toString() == "");
}

TEST_CASE("toString and increment", "[toString][increment]") {
  NgramCollectionHIGH g3;
  
  vector <string> v;
  v.push_back("Four");
  v.push_back("score");
  v.push_back("and");
  v.push_back("seven");
  
   g3.increment(v.begin(), v.end()-1);
   REQUIRE(g3.toString() == "Four score and \n");
   
   g3.increment(v.begin() +1, v.end());
   REQUIRE(g3.toString() == "Four score and \nscore and seven \n");

   g3.increment(v.begin(), v.end()-1);
   REQUIRE(g3.toString() == "Four score and \nscore and seven \n");
}

TEST_CASE("interior vector printing", "[increment][interiorVector]") {
 NgramCollectionHIGH g1;

 vector <string> v1;
 v1.push_back("The");
 v1.push_back("sky");

 g1.increment(v1.begin(), v1.end()-1);
 REQUIRE(g1.interiorVector() == "The_");

 g1.increment(v1.begin() +1, v1.end());
 REQUIRE(g1.interiorVector() == "The_sky_");

 g1.increment(v1.begin(), v1.end()-1);
 REQUIRE(g1.interiorVector() == "The_sky_The_");
}
