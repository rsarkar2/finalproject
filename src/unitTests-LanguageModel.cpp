/*
 */
#include "catch.hpp"
#include "LanguageModelHIGH.hpp"
#include "NgramCollectionHIGH.hpp"

#include<iostream>
#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;

TEST_CASE("This removes puntuation", "[removePunct]") {
  LanguageModelHIGH g3;
  string str = "Hello! My name, is jenn!! love me ;)";
  g3.removePunct(str);
  
  REQUIRE(g3.removePunct(str) == "hello my name is jenn love me ");
}

TEST_CASE("Detects the end of a sentence by capitalizing the first letter of the word & removes all punctuation", "[punctCheck]") {
  LanguageModelHIGH g3;
  string str = "t@od!!ay.";
  g3.punctCheck(str);
  
  REQUIRE(g3.punctCheck(str) == "Today");
}

TEST_CASE("Removes all punctuation", "[punctCheck]") {
  LanguageModelHIGH g3;
  string str = "t@od!!ay";
  g3.punctCheck(str);
  
  REQUIRE(g3.punctCheck(str) == "today");
}

/*
 *This test case tests the Medium option 
 */

TEST_CASE("adding Ngrams of length 3", "[increment], [setNgramLen], [toString], [addNGramsMED]") {
  LanguageModelHIGH g3;

  g3.setNgramLen(3);

  list<string> v;
  v.push_back("Christmas");
  v.push_back("Birthdays");
  v.push_back("NewYear");
  v.push_back("Thanksgiving");

  g3.addNGramsMED(v);
  REQUIRE(g3.toString() == "Birthdays NewYear Thanksgiving \nChristmas Birthdays NewYear \n");

  v.push_back("ZZZ");

  g3.addNGramsMED(v);
  REQUIRE(g3.toString() == "Birthdays NewYear Thanksgiving \nChristmas Birthdays NewYear \nNewYear Thanksgiving ZZZ \n");

}

/*
 * This test case tests the High option
 */
TEST_CASE("adding Ngrams of length 3 for addNGramsHigh", "[increment], [setNgramLen], [toString], [addNGramsHIGH]") {
  LanguageModelHIGH g2;

  g2.setNgramLen(3);

  list<string> v;
  v.push_back("It");
  v.push_back("is");
  v.push_back("the");
  v.push_back("most");
  v.push_back("Wonderful");
  v.push_back("time");
  v.push_back("of");
  v.push_back("year!");

  g2.addNGramsHIGH(v);
  REQUIRE(g2.toString() == "It is the most Wonderful time of year! \n");
  v.push_back("ZZZ");

  g2.addNGramsHIGH(v);
  REQUIRE(g2.toString() == "It is the most Wonderful time of year! \nIt is the most Wonderful time of year! ZZZ \n");
}

TEST_CASE("check if word is a small word", "[checkIfSmall}") {
  LanguageModelHIGH g2;

  string str = "coffee";
  REQUIRE(g2.checkIfSmall(str) == false);
 
  string str4 = "Caroline";
  REQUIRE(g2.checkIfSmall(str4) == false);
 
  string str1 = "a";
  REQUIRE(g2.checkIfSmall(str1) == true);

  string str2 = "the";
  REQUIRE(g2.checkIfSmall(str1) == true);

}

