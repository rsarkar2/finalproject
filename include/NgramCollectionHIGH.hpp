/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

#ifndef _CS120_NGRAM_COLLECTIONHIGH_HPP
#define _CS120_NGRAM_COLLECTIONHIGH_HPP

#include <sstream>
#include <iostream>
#include <istream>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>
#include <list>
#include <cstdlib> // for srand
#include <ctime> // for time() (for srand)

class NgramCollectionHIGH {
  friend class CompareFiles;

public:
  NgramCollectionHIGH() {};


  /*
  Increase count for NgramCollection representing values from begin up to end
  begin is an iterator to the first word in the Ngram,
  end is an iterator to the end of the Ngram
  */
  void increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end);

  /*
  Outputs all of the Ngrams together, one per line, without the numerical value at the end
  */
  std::string toString() const;

  /*
  Prints out the interior vector in the order of the file
  */
  std::string interiorVector() const;

private:

  /* map of all ngrams */
  std::map<std::string, std::map <std::vector<std::string>, int> > counts;

  /* order of words in the file */
  std::vector<std::string> nGramOrder;

};

#endif
