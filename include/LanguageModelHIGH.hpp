/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */
 
#ifndef _REENA_CS120_LANGUAGEMODEL_HIGH_HPP
#define _REENA_CS120_LANGUAGEMODEL_HIGH_HPP

#include "NgramCollectionHIGH.hpp"

#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>

#include <iostream> // for basic I/O
#include <fstream>  // for file I/O
#include <sstream>  // for string-streams (stream object hooked up to a std::string)
#include <iomanip>  // for precision control of output format


class LanguageModelHIGH {
  friend class CompareFiles;

public:
  LanguageModelHIGH() {};

  /* Sets the given file name to the overall file name. */
  void setFileName(std::string s);

  /* Sets the user-specified sensitivity level */
  void setCh(char c);

  /* Sets the nGram length (only for low and med sensitivity levels) */
  void setNgramLen(int n);

  /* Returns the Ngram in string format (using toString from NgramCollectionHIGH */
  std::string toString() const;

  /* Adds ngrams based on the sentences that are fed to it */
  void addNGramsHIGH(std::list<std::string> words);

  /* Adds ngrams based on the given nGram length (for med & low sensitivity) */
  void addNGramsMED(std::list<std::string> words);

  /* Reads the single file and sorts each nGram appropriately based on sentence length (high-level sensitivity) */
  void readSingleFileHIGH(std::string fileName); 

  /* Reads the single file and calls NgramMED to store specific nGram length */
  void readSingleFileMED(std::string fileName); 

  /* Checks if a given string is a "small word", outputs true if it is, else false.
  if the string is 6 or greater, it is automatically not a smal word */
  bool checkIfSmall(std::string word);

  /* Removes all punctuation from a word, if there is period/exclamation/question mark at the end of the sentence,
  then the first letter of that word is capitalized */
  std::string punctCheck(std::string text);

  /* Removes all punctuation from a word and changes any upper case words to lower case, returns the changes string (free of punct & uppercase) */
  std::string removePunct(std::string text);


private:
    /* the private nGramCollectionHIGH, a collection of Ngrams */
    NgramCollectionHIGH coll;

    /* the name of an individual file */
    std::string overallFile; 

    /* the user's choice */
    char ch; 

    /* length of each nGram, automatically set to 0 */
    int nGramLen = 0; 


};


#endif
