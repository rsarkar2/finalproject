/*
 * Name: Reena Sarkar & Jenn Baron
 * JH-ID: rsarkar2, jbaron11
 * Last Modified: 12/7/16
 */

#ifndef _CS120_COMPARE_FILES_HPP
#define _CS120_COMPARE_FILES_HPP

#include "LanguageModelHIGH.hpp"


#include <sstream>
#include <iostream>
#include <istream>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>
#include <list>
#include <cstdlib> // for srand
#include <ctime> // for time() (for srand)


class CompareFiles {

  public:

    CompareFiles() {};

    /* sets the character (whatever the user specifies) */
    void setCh(char c);

    /* sets the overall fileName (overallFile) */ 
    void setFileName(std::string s);

    std::string getFileName();

    /* this will read over the overall file and appropriately call either addFileLOW, addFileMED, or addFileHIGH */
    void readOverallFile();

    /* this stores ngrams for a (each have 3) - low level sensitivity */
    void addFileLOW(std::string file);

    /* this stores ngrams for a (each have 3) - medium level sensitivity */
    void addFileMED(std::string);

    /* 
    this stores the ngrams in sentences, so they all are of different size 
    it depends on where the sentence ends - high level sensitivity
    */
    void addFileHIGH(std::string);
    
    /* looks through the overall file and calls the correct 'find plagarism' based on what sensitivity level*/
    void findOverall();

    /* this compares 3-length nGrams, if 15% of the file is the same in a row, then returns 1. 
    else returns 0 */
    int findPlagarismLOW(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2);

    /* this compares 3-length nGrams, if 8% of the file is the same in a row, then returns 1.
    else returns 0 */
    int findPlagarismMED(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2);

    /* this compares sentences rather than 3-length nGrams, so the same words could be mixed up in a sentence - high level sensitivity*/
    int findPlagarismHIGH(std::string file1, NgramCollectionHIGH f1, std::string file2, NgramCollectionHIGH f2);

    /* this will compare the interior maps of 2 NgramCollectionHIGH's
    these are each a sentence in a file
    it will output 1 if the sentence is considered plagarised, otherwise it'll output 0 */
    int compareSentences(std::map<std::vector<std::string>, int> map1, std::map <std::vector<std::string>, int > map2);

    /* this compares the interior maps of 2 NgramCollectionHIGH's
    will output true if they're the same, else false */
    bool compareInterior(std::map<std::vector<std::string>, int> vect1, std::map <std::vector<std::string>, int > vect2);

    bool checkIfSmall(std::string word);
    //bool operator==(const std::vector<std::string> &other) const;

  private:

    /* vector of pairs, each pair containing a string and its associated NgramCollectionHIGH */
    std::vector< std::pair<std::string, NgramCollectionHIGH> > allFiles;
    
    /* the overall file name */
    std::string overallFile; //list of files filename

    /* user specified sensitivity level */
    char ch;

};


#endif
